FROM openjdk:11-jre-slim
ADD toolbox-qa-demo-1.0.0.jar demo.jar
RUN echo "PWD is: $PWD"
ENTRYPOINT [ "sh", "-c", "java -jar demo.jar $my_params cp " ]