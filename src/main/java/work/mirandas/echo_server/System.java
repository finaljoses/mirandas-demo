package work.mirandas.echo_server;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import work.mirandas.commons.BaseREST;
import work.mirandas.commons.URL;

import static io.restassured.RestAssured.given;

public class System extends BaseREST {

    public final String basePath = "/system/ping";

    @BeforeClass
    public void init() {
        RestAssured.baseURI = URL.ECHO_SERVER.getValue();
    }

    @Test(groups = {"system_ping", "happy"})
    public void OK() {
        Response response = given()
                .get(baseURI.concat(basePath));
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertTrue(response.jsonPath().getBoolean("ok"));
    }
}
