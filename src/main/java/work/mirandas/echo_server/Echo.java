package work.mirandas.echo_server;

import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import work.mirandas.commons.BaseREST;

import static io.restassured.RestAssured.given;

public class Echo extends BaseREST {
    public final String basePath = "/echo";

    @Test(groups = {"echo", "happy"})
    public void OK_String() {
        String text = " \"hello\"'hola'!#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_" +
                "`abcdefghijklmnopqrstuvwxyz{|}~";
        Response response = given()
                .param("text", text)
                .get(baseURI.concat(basePath));
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(response.jsonPath().getString("text"), text);
    }

    @Test(groups = {"echo", "happy"})
    public void OK_Int() {
        int text = 1244606645;
        Response response = given()
                .param("text", text)
                .get(baseURI.concat(basePath));
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(response.jsonPath().getString("text"), String.valueOf(text));
    }

    @Test(groups = {"echo", "sad"})
    public void Bad_Request() {
        Response response = given()
                .param("wrong", "")
                .get(baseURI.concat(basePath));
        assertBadRequest(response);
    }

    @Test(groups = {"echo", "sad"})
    public void Bad_Request_No_Param() {
        Response response = given()
                .get(baseURI.concat(basePath));
        assertBadRequest(response);
    }
}
