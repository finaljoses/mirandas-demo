package work.mirandas.echo_server;

import io.restassured.http.Header;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import work.mirandas.commons.BaseREST;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class Mobile extends BaseREST {
    private final Header expiredAuthorizationHeader = new Header("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJUb29sYm94TW9iaWxlVGVzdCIsIm5hbWUiOiJUb29sYm94IEF1dGggdGVzdCIsImlhdCI6MTU4NjMwNjYyMCwiZXhwaXJlRGF0ZSI6IjIwMjAtMDQtMDhUMDA6NDM6NDAuNjY2WiJ9.YWgXynQ9ZtVYExsKiHuyjuSkNchzCFcrHikgbHAEzzw");
    private final String basePath = "/mobile";
    private Header contentTypeJSON = new Header("Content-Type", "application/json");
    private Header authorizationHeader;

    @Test(groups = {"mobile", "sad"})
    public void Echo_Unauthorized() {
        Response response = given()
                .get(baseURI.concat(basePath).concat("/echo"));
        assertUnauthorized(response);
    }

    @Test(groups = {"mobile", "sad"})
    public void Data_Unauthorized() {
        Response response = given()
                .get(baseURI.concat(basePath).concat("/data"));
        assertUnauthorized(response);
    }

    @Test(groups = {"mobile", "sad"})
    public void Auth_Unauthorized_No_Value() {
        Response response = given()
                .body("")
                .post(baseURI.concat(basePath).concat("/auth"));
        Assert.assertEquals(response.statusCode(), 401);
        Assert.assertEquals(response.jsonPath().getString("code"), "E401");
        Assert.assertEquals(response.jsonPath().getString("message"), "Unauthorized");
    }

    @Test(groups = {"mobile", "sad"})
    public void Auth_Unauthorized_Wrong_Parameter_Value() {
        Response response = given()
                .body("{\"sub\": 123}")
                .post(baseURI.concat(basePath).concat("/auth"));
        Assert.assertEquals(response.statusCode(), 401);
        Assert.assertEquals(response.jsonPath().getString("code"), "E401");
        Assert.assertEquals(response.jsonPath().getString("message"), "Unauthorized");
    }

    @Test(groups = {"mobile", "happy"})
    public void Auth_OK() {
        createAuthToken();
    }

    @Test(dependsOnMethods = "Auth_OK", priority = 5, groups = {"mobile", "happy"})
    public void Echo_OK_String() {
        String text = " \"hello\"'hola'!#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_" +
                "`abcdefghijklmnopqrstuvwxyz{|}~";

        Response response = given()
                .header(contentTypeJSON)
                .header(authorizationHeader)
                .param("text", text)
                .get(baseURI.concat(basePath).concat("/echo"));
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(response.jsonPath().getString("text"), text);
    }

    @Test(dependsOnMethods = "Auth_OK", priority = 5, groups = {"mobile", "happy"})
    public void Echo_OK_Int() {
        int text = 1994406609;
        Response response = given()
                .header(contentTypeJSON)
                .header(authorizationHeader)
                .param("text", text)
                .get(baseURI.concat(basePath).concat("/echo"));
        Assert.assertEquals(response.statusCode(), 200);
        Assert.assertEquals(response.jsonPath().getString("text"), String.valueOf(text));
    }

    @Test(priority = 5, groups = {"mobile", "sad"})
    public void Echo_Bad_Request() {
        createAuthToken();
        Response response = given()
                .header(contentTypeJSON)
                .header(authorizationHeader)
                .param("wrong", "")
                .get(baseURI.concat(basePath).concat("/echo"));
        assertBadRequest(response);
    }

    @Test(priority = 5, groups = {"mobile", "sad"})
    public void Echo_Bad_Request_No_Param() {
        createAuthToken();
        Response response = given()
                .header(contentTypeJSON)
                .header(authorizationHeader)
                .get(baseURI.concat(basePath).concat("/echo"));
        assertBadRequest(response);
    }

    @Test(dependsOnMethods = "Auth_OK", priority = 5, groups = {"mobile", "happy"})
    public void Data_OK() {
        Response response = given()
                .header(contentTypeJSON)
                .header(authorizationHeader)
                .get(baseURI.concat(basePath).concat("/data"));
        Assert.assertEquals(response.statusCode(), 200);
        response.then().assertThat().body(matchesJsonSchemaInClasspath("schemas/mobile_data.json"));
    }

    @Test(priority = 90, groups = {"mobile", "sad"})
    public void Auth_Token_Unauthorized() {
        Response responseData = given()
                .header("Authorization", "accept")
                .get(baseURI.concat(basePath).concat("/data"));
        Response responseEcho = given()
                .header("Authorization", "accept")
                .get(baseURI.concat(basePath).concat("/echo"));
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(responseData.statusCode(), 401);
        softAssert.assertEquals(responseEcho.statusCode(), 401);
        softAssert.assertEquals(responseData.jsonPath().getString("code"), "E402");
        softAssert.assertEquals(responseEcho.jsonPath().getString("code"), "E402");
        softAssert.assertAll();
    }

    @Test(priority = 90, groups = {"mobile", "sad"})
    public void Auth_Token_Expired() {
        Response responseData = given()
                .header(contentTypeJSON)
                .header(expiredAuthorizationHeader)
                .get(baseURI.concat(basePath).concat("/data"));
        Response responseEcho = given()
                .header(contentTypeJSON)
                .header(expiredAuthorizationHeader)
                .get(baseURI.concat(basePath).concat("/echo"));
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(responseData.statusCode(), 401);
        softAssert.assertEquals(responseEcho.statusCode(), 401);
        softAssert.assertEquals(responseData.jsonPath().getString("code"), "E403");
        softAssert.assertEquals(responseEcho.jsonPath().getString("code"), "E403");
        softAssert.assertAll();
    }

    private void assertUnauthorized(Response response) {
        Assert.assertEquals(response.statusCode(), 401);
        Assert.assertEquals(response.jsonPath().getString("code"), "E401");
        Assert.assertEquals(response.jsonPath().getString("message"), "Authorization required");
    }

    private void createAuthToken() {
        Response response = given()
                .header(contentTypeJSON)
                .body("{\"sub\" : \"ToolboxMobileTest\"}")
                .post(baseURI.concat(basePath).concat("/auth"));
        Assert.assertEquals(response.statusCode(), 200);
        response.then().assertThat().body(matchesJsonSchemaInClasspath("schemas/mobile_auth.json"));
        String authToken = response.jsonPath().getString("token");
        String authType = response.jsonPath().getString("type");
        authorizationHeader = new Header("Authorization", authType.concat(" ").concat(authToken));
    }
}
