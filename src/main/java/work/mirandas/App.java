package work.mirandas;

import org.testng.TestNG;
import org.testng.xml.*;

import java.util.ArrayList;
import java.util.List;

public class App {
    final static String HAPPY = "happy";
    final static String SAD = "sad";

    public static void main(String[] args) {
        XmlGroups xmlGroups = new XmlGroups();
        XmlRun xmlRun = new XmlRun();
        if (args.length == 0) {
            System.err.println("Missing arguments. View docs for more information.");
            return;
        } else if (args.length > 2) {
            System.err.println("Too many arguments. Expected 2 maximum but found: [" + args.length + "].\n" +
                    "View docs for more information.");
            return;
        } else {
            xmlRun.onInclude(args[0]);
            if (args.length == 2) {
                String exclude = args[1].equals(HAPPY) ? SAD : HAPPY;
                xmlRun.onExclude(exclude);
            }
        }
        xmlGroups.setRun(xmlRun);

        XmlSuite suite = new XmlSuite();
        suite.setName("My Demo Suite");
        suite.addListener("org.uncommons.reportng.HTMLReporter");
        suite.addListener("org.uncommons.reportng.JUnitXMLReporter");

        List<XmlPackage> packages = new ArrayList<>();
        packages.add(new XmlPackage("work.mirandas.echo_server"));

        XmlTest test = new XmlTest(suite);
        test.setName("A Simple Test");
        test.setGroups(xmlGroups);
        test.setXmlPackages(packages);

        List<XmlSuite> suites = new ArrayList<>();
        suites.add(suite);

        TestNG testNG = new TestNG();
        testNG.setUseDefaultListeners(false);
        testNG.setXmlSuites(suites);
        testNG.run();
    }
}
