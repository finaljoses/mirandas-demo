package work.mirandas.commons;

public enum URL {
    ECHO_SERVER("https://echo-serv.tbxnet.com/v1");

    private String url;

    URL(String s) {
        this.url = s;
    }

    public String getValue() {
        return url;
    }
}
