package work.mirandas.commons;

import io.restassured.response.Response;
import org.testng.Assert;

public class BaseREST {
    protected String baseURI = URL.ECHO_SERVER.getValue();

    protected void assertBadRequest(Response response) {
        Assert.assertEquals(response.statusCode(), 400);
        Assert.assertEquals(response.jsonPath().getString("code"), "E400");
        Assert.assertEquals(response.jsonPath().getString("message"), "Bad Request");
    }
}
